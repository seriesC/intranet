<? 
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard Template for Bootstrap</title>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="dashboard.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">SeriesC</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right hidden-sm hidden-md hidden-lg">
                        <li>
                            <a href="/seriesC/summary.php">요약</a>
                        </li>
                        <li>
                            <a href="/seriesC/usage.php">공동카드 사용</a>
                        </li>
                        <li>
                            <a href="/seriesC/input.php">공동카드 입금</a>
                        </li>
                        <li>
                            <a href="/seriesC/priUsage.php">개인카드 사용</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
        <!--  하이 헬로 -->
        
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active">
                            <a href="/seriesC/summary.php">요약</a>
                        </li>
                        <li>
                            <a href="/seriesC/usage.php">공동카드 사용</a>
                        </li>
                        <li>
                            <a href="/seriesC/input.php">공동카드 입금</a>
                        </li>
                         <li>
                            <a href="/seriesC/priUsage.php">개인카드 사용</a>
                        </li>
                    </ul>
                </div>
               <?  
    $db_host        = 'localhost';
	$db_user        = 'r54r45r54';
	$db_pass        = 'e34e43E34'; 
	$db_database    = 'r54r45r54'; 
	$link=mysql_connect($db_host,$db_user,$db_pass);
	mysql_select_db($db_database,$link);
	$wooSum=0;$suSum=0;$daeSum=0;$jinSum=0;
	
	$query="select money from woo;";
	$result=mysql_query($query);
	while($row=mysql_fetch_array($result)){
		$wooSum+=$row[0];
		}
	$query="select money from su;";
	$result=mysql_query($query);
	while($row=mysql_fetch_array($result)){
		$suSum+=$row[0];
		}
		$query="select money from dae;";
	$result=mysql_query($query);
	while($row=mysql_fetch_array($result)){
		$daeSum+=$row[0];
		}
		$query="select money from jin;";
	$result=mysql_query($query);
	while($row=mysql_fetch_array($result)){
		$jinSum+=$row[0];
		}
		
	$total=$wooSum+$suSum+$daeSum+$jinSum;
	
	?>
                <div class="col-sm-9 col-sm-offset-3 main">
                    <h2 class="page-header">공동 카드 잔액 요약 (총 <? echo $total; ?> 원)</h2>
                    <div class="row">
                        <div class="" style="display: block;">
                            <div class="progress active progress-striped col-xs-push-0 col-xs-pull-0 col-xs-offset-0" style="display: block;"> 
                                <div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: <? echo $wooSum/$total*100; ?>%;">
                                    <span class="label label-default">김우현</span> 
                                     
                                </div>
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: <? echo $suSum/$total*100; ?>%; ">
                                    <span class="label label-default">류수연</span> 
                                   
                                </div>
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: <? echo $daeSum/$total*100; ?>%; ">
                                    <span class="label label-default">박대영</span> 
                                   
                                </div>
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: <? echo $jinSum/$total*100; ?>%; ">
                                    <span class="label label-default">천진우</span> 
                                  
                                </div>                                 
                            </div>                             
                        </div>
                    </div>
                     
                    <div class="row placeholders">
                        <div class="col-xs-6 col-sm-3 placeholder">
                            <h4>김우현</h4>
                            <span class="text-muted"><? echo $wooSum ?> 원</span>
                        </div>
                        <div class="col-xs-6 col-sm-3 placeholder">
                            <h4>류수연</h4>
                            <font color="#777777"><? echo $suSum ?>  원</font>
                        </div>
                        <div class="col-xs-6 col-sm-3 placeholder">
                            <h4>박대영</h4>
                            <span class="text-muted"><? echo $daeSum ?>  원</span>
                        </div>
                        <div class="col-xs-6 col-sm-3 placeholder">
                            <h4>천진우</h4>
                            <span class="text-muted"><? echo $jinSum ?>  원</span>
                        </div>
                    </div>
                    <h2 class="sub-header">공동 카드 최근 내역</h2>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>날짜</th>
                                    <th>내역</th>
                                    <th>참여</th>
                                    <th>금액</th>
                                </tr>
                            </thead>
                            <tbody style="display: table-row-group;">
                          <?  
    $db_host        = 'localhost';
	$db_user        = 'r54r45r54';
	$db_pass        = 'e34e43E34'; 
	$db_database    = 'r54r45r54'; 
	$link=mysql_connect($db_host,$db_user,$db_pass);
	mysql_select_db($db_database,$link);
	
	
	$query="select * from summaryList order by idx";
	$result=mysql_query($query);
	while($row=mysql_fetch_array($result)){
		$name="";
		if($row[2]=="woo")$name="김우현";
		elseif($row[2]=="su")$name="류수연";
		elseif($row[2]=="dae")$name="박대영";
		elseif($row[2]=="jin") $name="천진우";
		else $name=$row[2];
	?>
                                <tr style="display: table-row;">
                                    <td><? echo $row[0]; ?></td>
                                    <td><? echo $row[1]; ?></td>
                                    <td><? echo $name; ?></td>
                                    <td><? echo $row[3]; ?> 원</td>
                                </tr>
                                <? } ?>
                            </tbody>
                        </table>
                    </div>
                        <h2 class="sub-header" style="display: block;">개인 송금 요청</h2>
                    <div class="table-responsive" style="display: block;">
                        <table class="table table-striped">
                            <thead style="display: table-header-group;">
                                <tr style="display: table-row;">
                                	<th>내역</th>
                                    <th>보낼 사람</th>
                                    <th>받을 사람</th>
                                    <th>금액</th>
                                    <th style="display: table-cell;">송금 완료</th>
                                </tr>
                            </thead>
                            <tbody style="display: table-row-group;">
                            <?  
    $db_host        = 'localhost';
	$db_user        = 'r54r45r54';
	$db_pass        = 'e34e43E34'; 
	$db_database    = 'r54r45r54'; 
	$link=mysql_connect($db_host,$db_user,$db_pass);
	mysql_select_db($db_database,$link);
	
	$query="select * from loan;";
	$result=mysql_query($query);
	$wootosu=0;
	$wootodae=0;
	$wootojin=0;
	$sutodae=0;
	$sutojin=0;
	$daetojin=0;
	
	while($row=mysql_fetch_array($result)){ 
	
		if($row[1]!=$row[2]&&$row[6]!='1'){
			
			$name="";
		if($row[2]=="woo")$name="김우현";
		elseif($row[2]=="su")$name="류수연";
		elseif($row[2]=="dae")$name="박대영";
		elseif($row[2]=="jin") $name="천진우";
		else $name=$row[2];
		
		$name2="";
		if($row[1]=="woo"){$name2="김우현";
			if($row[2]=="su")$wootosu+=$row[3];
			else if($row[2]=="dae")$wootodae+=$row[3];
			else if($row[2]=="jin")$wootojin+=$row[3];
		}
		elseif($row[1]=="su"){$name2="류수연";
			if($row[2]=="woo")$wootosu-=$row[3];
			else if($row[2]=="dae")$sutodae+=$row[3];
			else if($row[2]=="jin")$sutojin+=$row[3];
			}
		elseif($row[1]=="dae"){$name2="박대영";
			if($row[2]=="woo")$wootodae-=$row[3];
			else if($row[2]=="su")$sutodae-=$row[3];
			else if($row[2]=="jin")$daetojin+=$row[3];
			}
		elseif($row[1]=="jin"){ $name2="천진우";
					if($row[2]=="woo")$wootojin-=$row[3];
			else if($row[2]=="su")$sutojin-=$row[3];
			else if($row[2]=="dae")$daetojin-=$row[3];
			}
		else $name2=$row[1];
		}}
		
	?>
						 <tr style="display: table-row;">
                                	<td>총합</td>
                                    <td><? if($wootosu>0)echo "김우현"; else echo "류수연"; ?></td>
                                    <td><? if($wootosu>0)echo "류수연"; else echo "류수연"; ?></td>
                                    <td><? if($wootosu>0)echo $wootosu; else echo $wootosu*(-1); ?></td>
                                    <td style="display: table-cell;">
                                        <button type="button" onclick="location.href='/seriesC/priUsageCompleteFunc.php?idx='" class="btn btn-block btn-primary btn-xs" style="display: inline-block;">송금 완료</button>
                                       
                                    </td>
                                </tr>
	
                             <?  
    $db_host        = 'localhost';
	$db_user        = 'r54r45r54';
	$db_pass        = 'e34e43E34'; 
	$db_database    = 'r54r45r54'; 
	$link=mysql_connect($db_host,$db_user,$db_pass);
	mysql_select_db($db_database,$link);
	
	$query="select * from loan;";
	$result=mysql_query($query);
	$wootosu,$wootodae,$wootojin,$sutodae,$sutojin,$daetojin=0;
	
	while($row=mysql_fetch_array($result)){ 
	
		if($row[1]!=$row[2]&&$row[6]!='1'){
			
			$name="";
		if($row[2]=="woo")$name="김우현";
		elseif($row[2]=="su")$name="류수연";
		elseif($row[2]=="dae")$name="박대영";
		elseif($row[2]=="jin") $name="천진우";
		else $name=$row[2];
		
		$name2="";
		if($row[1]=="woo"){$name2="김우현";
			if($row[2]=="su")$wootosu+=$row[3];
			else if($row[2]=="dae")$wootodae+=$row[3];
			else if($row[2]=="jin")$wootojin+=$row[3];
		}
		elseif($row[1]=="su"){$name2="류수연";
			if($row[2]=="woo")$wootosu-=$row[3];
			else if($row[2]=="dae")$sutodae+=$row[3];
			else if($row[2]=="jin")$sutojin+=$row[3];
			}
		elseif($row[1]=="dae"){$name2="박대영";
			if($row[2]=="woo")$wootodae-=$row[3];
			else if($row[2]=="su")$sutodae-=$row[3];
			else if($row[2]=="jin")$daetojin+=$row[3];
			}
		elseif($row[1]=="jin"){ $name2="천진우";
					if($row[2]=="woo")$wootojin-=$row[3];
			else if($row[2]=="su")$sutojin-=$row[3];
			else if($row[2]=="dae")$daetojin-=$row[3];
			}
		else $name2=$row[1];
	?>
                                <tr style="display: table-row;">
                                	<td><? echo $row[4]; ?></td>
                                    <td><? echo $name2; ?></td>
                                    <td><? echo $name; ?></td>
                                    <td><? echo $row[3]; ?></td>
                                    <td style="display: table-cell;">
                                        <button type="button" onclick="location.href='/seriesC/priUsageCompleteFunc.php?idx=<? echo $row[0]; ?>'" class="btn btn-block btn-primary btn-xs" style="display: inline-block;">송금 완료</button>
                                       
                                    </td>
                                </tr>
                                <? }} ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
