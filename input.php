<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard Template for Bootstrap</title>
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="dashboard.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">SeriesC</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right hidden-sm hidden-md hidden-lg">
                         <li>
                            <a href="/seriesC/summary.php">요약</a>
                        </li>
                        <li>
                            <a href="/seriesC/usage.php">공동카드 사용</a>
                        </li>
                        <li>
                            <a href="/seriesC/input.php">공동카드 입금</a>
                        </li>
                        <li>
                            <a href="/seriesC/priUsage.php">개인카드 사용</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="">
                            <a href="/seriesC/summary.php">요약</a>
                        </li>
                        <li>
                            <a href="/seriesC/usage.php">공동카드 사용</a>
                        </li>
                        <li class="active">
                            <a href="/seriesC/input.php" class="">공동카드 입금</a>
                        </li>
                        <li>
                            <a href="/seriesC/priUsage.php">개인카드 사용</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-9 col-sm-offset-3 main">
                    <h2 class="page-header">입금</h2>
                    <form style="display: block;" method="post" action="inputFunc.php">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered">
                                <thead style="display: table-header-group;">
                                    <tr class="info" style="display: table-row;">
                                        <th>날짜</th>
                                        <th>입금자</th>
                                    </tr>
                                </thead>
                                <tbody style="display: table-row-group;">
                                    <tr style="display: table-row;">
                                        <td style="display: table-cell;">
                                            <input type="date" name="date" class="form-control" placeholder="Placeholder text" style="display: inline-block;">
                                        </td>
                                        <td>
                                            <div class="radio">
                                                <div class="row">
                                                    <div class="col-xs-12" style="display: block;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4" style="display: block;">
                                                                <label class="control-label">
                                                                    <input type="radio" name="who" value="woo"  style="display: block;">                                                                     
                                                                    김우현
                                                                </label>                                                                 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4" style="display: block;">
                                                                <label class="control-label">
                                                                    <input type="radio"  name="who" value="su"  style="display: block;">     
                                                                                                                                                                                                   류수연
                                                                </label>                                                                 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4" style="display: block;">
                                                                <label class="control-label">
                                                                    <input type="radio"  name="who" value="dae"  style="display: block;">      
                                                                                                                                                                                                     박대영
                                                                </label>                                                                 
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4" style="display: block;">
                                                                <label class="control-label">
                                                                    <input type="radio" name="who"  value="jin"  style="display: block;">   
                                                                                                                                                                                                         천진우
                                                                </label>                                                                 
                                                            </div>
                                                        </div>                                                         
                                                    </div>
                                                </div>                                                 
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="info" style="display: table-row;">
                                        <th rowspan="2">입금액</th>
                                    </tr>
                                    <tr style="display: table-row;">
                                        <td>
                                            <input type="number" name="money" class="form-control input-sm" style="display: block;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    
                    <button type="submit" class="btn btn-block btn-primary btn-lg" data-trigger="hover" style="display: inline-block;">제출</button>
                </form>
                </div>
            </div>
        </div>
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
